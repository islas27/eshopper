var tienda = angular.module('tienda', []);


tienda.config(function ($routeProvider) {
    $routeProvider
        .when('/',
            {
                templateUrl: 'views/home.html'
                , controller: 'home'
            }
        )
        .when('/detalle/:id',
            {
                controller: 'detalle',
                templateUrl: 'views/detalle.html'
            })

        .when('/cart',
            {
                controller: 'cart',
                templateUrl: 'views/cart.html'
            })

        .otherwise({redirectTo: '/'});
});


// Definicion de clases

var Carrito = function () {
    this.entradas = [];
    this.subTotal = 0;
    this.iva = 0;
    this.total = 0;
};

var Entrada = function () {
    this.producto = {};
    this.canti = 0;
};

Entrada.prototype.getImporte = function () {
    return Math.abs(Math.round(this.canti)) * this.producto.precio;
};

Entrada.prototype.increCanti = function () {
    this.canti++;
};

Entrada.prototype.decreCanti = function () {
    if(this.canti > 0) this.canti--;
};

Entrada.prototype.setCanti = function (n) {
    this.canti = Math.abs(Math.round(n));
};

Entrada.prototype.setCantiPlus = function (n) {
    this.canti += Math.abs(Math.round(n));
};

Entrada.prototype.setProd = function (p) {
    this.producto = p;
};

var User = function () {
    this.nombre = "";
    this.carrito = new Carrito();
};

var user1 = new User();
user1.nombre = "Juan";

Carrito.prototype.totalizar = function () {
    this.subTotal = 0;
    for (var i = 0; i < this.entradas.length; i++)
        this.subTotal += this.entradas[i].getImporte();
    this.iva = this.subTotal * .16;
    this.total = this.iva + this.subTotal;
};

Carrito.prototype.addProd = function (p, c) {
    var esta = false;
    for (var i = 0; i < this.entradas.length; i++) {
        if (p.nombre == this.entradas[i].producto.nombre) {
            this.entradas[i].setCantiPlus(c);
            esta = true;
        }
    }
    if (!esta) {
        var tmp = new Entrada();
        angular.copy(p, tmp.producto);
        tmp.setCanti(c);
        this.entradas.push(tmp);
    }
    console.log(user1);
    this.totalizar();
};

//-----------------------------------

tienda.factory("global",
    function ($rootScope, $http) {
        var obj = {};

        // in not session
        obj.productos = [];
        obj.camisetas = [];


        // in session
        obj.user = user1;

        obj.addToCar = function (p) {
            obj.user.carrito.addProd(p, 1);
            $rootScope.$broadcast("addToCar");
        };

        obj.delToCar = function (nom) {
            for (var i = 0; i < obj.user.carrito.entradas.length; i++) {
                if (obj.user.carrito.entradas[i].nombre == nom) {
                    obj.user.carrito.entradas.splice(1, i);
                    $rootScope.$broadcast("addToCar");
                    break;
                }
            }
        };

        obj.getCamisetas = function () {

            if (obj.productos.length > 0)
                $rootScope.$broadcast("getCamisetas");
            else
                $http.get("/camisetas").success(
                    function (data) {
                        obj.camisetas = data;
                        $rootScope.$broadcast("getCamisetas");
                    }
                );
        };

        obj.getProductos = function () {

            if (obj.productos.length > 0)
                $rootScope.$broadcast("getProductos");
            else
                $http.get("/productos").success(
                    function (data) {
                        obj.productos = data;
                        $rootScope.$broadcast("getProductos");
                    }
                );
        };

        obj.getProducto = function (id) {
            for (var i = 0; i < obj.productos.length; i++) {
                if (obj.productos[i].id == id)
                    return obj.productos[i];
            }
        };

        obj.getEntradas = function () {
            if (obj.user.carrito.entradas.length > 0)
                $rootScope.$broadcast("getEntradas");
            else
                $http.get("/entradas").success(
                    function (data) {
                        for (var i = 0; i < data.length; i++) {
                            obj.user.carrito.addProd(data[i].producto, data[i].canti);
                        }
                        $rootScope.$broadcast("getEntradas");
                    }
                );
        };

        return obj;
    }
);

function main($scope, global) {
    console.log("main");
}


function cart($scope, global) {
    console.log("cart");

    $scope.$on("getEntradas",
        function () {
            $scope.lista = global.user.carrito.entradas;
        }
    );

    $scope.addToCar = function (e) {
        global.addToCar(e);
    };

    $scope.lista =[];

    global.getEntradas();

}


function detalle($scope, $routeParams, $location, global) {
    $scope.id = $routeParams.id;
    $scope.prod = global.getProducto($scope.id);

    if (!$scope.prod)
        $location.path('/');


    $scope.addToCar = function (e) {
        global.addToCar(e);
    }
}


function home($scope, global) {
    console.log("home");

    $scope.$on("getProductos",
        function () {
            $scope.productos = global.productos;
        }
    );

    $scope.$on("getCamisetas",
        function () {
            $scope.camisetas = global.camisetas;
        }
    );

    $scope.productos = [];
    $scope.camisetas = [];

    global.getProductos();
    global.getCamisetas();


}

tienda.controller("main", main);
tienda.controller("cart", cart);
tienda.controller("home", home);
tienda.controller("detalle", detalle);

