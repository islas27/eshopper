package controllers;

import play.*;
import play.mvc.*;

import java.util.*;

import models.*;
import play.mvc.results.RenderJson;
import play.server.PlayHandler;

public class Application extends Controller {

    public static void Productos() {
        List<Product> allP = Product.find().fetch();
        renderJSON(allP);
    }

    public static void Camisetas() {
        List<Camiseta> allC = Camiseta.find().fetch();
        renderJSON(allC);
    }

    public static void Entradas() {
        List<Entrada> allE = Entrada.find().fetch();
        renderJSON(allE);
    }


}