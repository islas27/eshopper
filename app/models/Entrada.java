package models;

import play.modules.mongo.MongoEntity;
import play.modules.mongo.MongoModel;

/**
 * Created by jonathan on 2/29/16.
 */

@MongoEntity("entradas")
public class Entrada extends MongoModel {

    public Product producto;
    public int  canti;

}