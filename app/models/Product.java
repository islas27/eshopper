package models;

import play.modules.mongo.MongoEntity;
import play.modules.mongo.MongoModel;

/**
 * Created by jonathan on 2/29/16.
 */

@MongoEntity("productos")
public class Product extends MongoModel {

    public int id;
    public String nombre;
    public double precio;
    public String img;

    @Override
    public String toString(){
        return nombre + "[$" + precio + "]";
    }
}
